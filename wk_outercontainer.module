<?php

/**
 * @file
 * Contains wk_outercontainer.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function wk_outercontainer_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the wk_outercontainer module.
    case 'help.page.wk_outercontainer':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Outer Container') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_page_attachments().
 */
function wk_outercontainer_page_attachments(array &$page) {
  $page['#attached']['library'][] = 'wk_outercontainer/admin';
}

/**
 * Implements hook_preprocess_ds_entity_view().
 */
function wk_outercontainer_preprocess_ds_entity_view(&$variables) {
  if (isset($variables['content']['#paragraph'])) {

    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    $paragraph = $variables['content']['#paragraph'];

    if ($paragraph->bundle() === 'outer_container') {
      $styles = [];

      _wk_outercontainer_background_image('field_oc_background_image', $paragraph, $styles);
      _wk_outercontainer_vertical_spacing('field_oc_vertical_spacing', $paragraph, $styles);
      _wk_outercontainer_background_color('field_oc_background_color', $paragraph, $styles);
      _wk_outercontainer_minimal_height('field_oc_minimal_height', $paragraph, $styles, $variables);
      _wk_outercontainer_invert_color('field_oc_invert_text_color', $paragraph, $variables);

      $variables['content']['#settings']['attributes'] = 'style|' . implode(';', $styles);
    }
  }
}

/**
 * Adding background image to paragraph.
 *
 * @param string $field
 *   Field name as a string.
 * @param object $paragraph
 *   The paragraph object.
 * @param array $styles
 *   Styles array.
 */
function _wk_outercontainer_background_image($field, $paragraph, array &$styles) {
  if ($paragraph->hasField($field)) {
    if ($paragraph->get($field)->referencedEntities()) {
      $background_image = $paragraph->get($field)->referencedEntities();

      if (isset($background_image[0])) {

        /** @var Drupal\file\Entity\File $image */
        $image = $background_image[0];

        $styles[] = 'background-image:url(' . file_create_url($image->getFileUri()) . ')';
        $styles[] = 'background-size:cover';
        $styles[] = 'background-position:center center';
      }
    }
  }
}

/**
 * Adding vertical padding to paragraph.
 *
 * @param string $field
 *   Field name as a string.
 * @param object $paragraph
 *   The paragraph object.
 * @param array $styles
 *   Styles array.
 */
function _wk_outercontainer_vertical_spacing($field, $paragraph, array &$styles) {
  if ($paragraph->hasField($field)) {
    if (isset($paragraph->get($field)->value)) {
      $value = intval($paragraph->get($field)->value);
      $styles[] = 'padding-top:' . $value . 'px';
      $styles[] = 'padding-bottom:' . $value . 'px';
    }
  }
}

/**
 * Adding background color to paragraph.
 *
 * @param string $field
 *   Field name as a string.
 * @param object $paragraph
 *   The paragraph object.
 * @param array $styles
 *   Styles array.
 */
function _wk_outercontainer_background_color($field, $paragraph, array &$styles) {
  if ($paragraph->hasField($field) && $paragraph->get($field)->first() !== NULL) {

    /** @var Drupal\taxonomy\TermInterface $taxonomy_color */
    $taxonomy_color = $paragraph->get($field)
      ->first()
      ->get('entity')
      ->getTarget();
    $styles[] = 'background-color:' . $taxonomy_color->get('field_hex_value')->value;
  }
}

/**
 * Add extra class to outer container.
 *
 * @param string $field
 *   Field name as a string.
 * @param object $paragraph
 *   The paragraph object.
 * @param array $variables
 *   Variables array.
 */
function _wk_outercontainer_invert_color($field, $paragraph, array &$variables) {
  if ($paragraph->hasField($field)) {
    if (isset($paragraph->get($field)->value) && $paragraph->get($field)->value <> 0) {
      $variables['content']['#settings']['classes']['layout_class'][] = 'invert-color';
    }
  }
}

/**
 * Adding min-height to paragraph.
 *
 * @param string $field
 *   Field name as a string.
 * @param object $paragraph
 *   The paragraph object.
 * @param array $styles
 *   Styles array.
 * @param array $variables
 *   Variables array.
 */
function _wk_outercontainer_minimal_height($field, $paragraph, array &$styles, array &$variables) {
  if ($paragraph->hasField($field)) {
    if (isset($paragraph->get($field)->value)) {
      $styles[] = 'min-height:' . intval($paragraph->get($field)->value) . 'px';
      $variables['content']['#settings']['classes']['layout_class'][] = 'vertical-align';
    }
  }
}
